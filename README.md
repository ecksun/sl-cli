This is a simple library/cli that accesses SL's web APIs for some basic
information. I intend to use it as both a library and as a CLI tool.

The big difference between this and sl-api is that no API keys are required.
This project uses the APIs SL webpage uses. This also has the implication that
the project might break at any time.
