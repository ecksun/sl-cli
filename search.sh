#!/bin/bash

search() {
    local q="$1"
    local response=$(curl -s "http://sl.se/api/TypeAhead/Find/$q")
    local data=$(echo "$response" | jq .data)

    local n=$(echo "$data" | jq --raw-output '. | length')

    for i in $(seq 0 $(($n - 1))); do
        local item=$(echo "$data" | jq --arg i $i '.[$i | tonumber]')

        local name=$(echo "$item" | jq --raw-output '.Name')
        local site_id=$(echo "$item" | jq --raw-output '.SiteId')
        local type=$(echo "$item" | jq --raw-output '.Type')
        local coords=$(echo "$item" | jq --raw-output '.Sid')
        echo "$name [$type] @ $coords - $site_id"
    done
}

realtime() {
    local site_id="$1"
    local response=$(curl "http://sl.se/api/sv/RealTime/GetDepartures/$site_id");
    echo "$response" | jq .
}

search "$1"
