'use strict';
const MetroColor = require('../lib/sl').MetroColor;
const TravelType = require('../lib/sl').TravelType;

function trainDeparture(lineNumber, destination, time, deviation) {
    const deviations = deviation ? [{ text: deviation }] : [];
    return {
        lineNumber: lineNumber,
        destination: destination,
        time: time,
        deviations: deviations,
        type: TravelType.TRAIN
    };
}

function metroDeparture(lineNumber, color, destination, time, message) {
    return {
        lineNumber: lineNumber,
        line: getMetroLine(color),
        destination: destination,
        time: time,
        message: message,
        color: color,
        type: TravelType.METRO
    };
}

function getMetroLine(color) {
    switch (color) {
        case MetroColor.RED: return 'Tunnelbanans röda linje';
        case MetroColor.GREEN: return 'Tunnelbanans gröna linje';
        case MetroColor.BLUE: return 'Tunnelbanans blå linje';
    }
}

module.exports = {
    trainDeparture: trainDeparture,
    metroDeparture: metroDeparture
};
