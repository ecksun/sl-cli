'use strict';

const assert = require('assert');
const sinon = require('sinon');

const CLI = require('../lib/cli');
const MetroColor = require('../lib/sl').MetroColor;

const train = require('./util').trainDeparture;
const metro = require('./util').metroDeparture;

describe('CLI', function() {
    it('Print realtime updates', function() {
        const cli = new CLI({
            find: sinon.stub().returns(Promise.resolve([{
                name: 'Fridhemsplan (Stockholm)',
                siteId: 9115,
                type: 'station',
                coords: null
            }])),
            realtime: sinon.stub().withArgs(9115).returns(Promise.resolve({
                departures: {
                    metro: [
                        metro(10, MetroColor.BLUE, 'Hjulsta', '9 min', ''),
                        metro(11, MetroColor.BLUE, 'Akalla', '23:07', 'Kort tåg'),
                        metro(10, MetroColor.BLUE, 'Kungsträdgården', '11 min', ''),
                    ],
                    train: [],
                    bus: [],
                    ship: []
                }
            })),
        });
        return cli.realtime('fridhemsplan')
        .then(data => assert.equal(data, [
            'Tunnelbanans blå linje 10 mot Hjulsta - 9 min',
            'Tunnelbanans blå linje 11 mot Akalla - 23:07 (Kort tåg)',
            'Tunnelbanans blå linje 10 mot Kungsträdgården - 11 min'
        ].join('\n')));
    });
    it('should have train information', function() {
        const cli = new CLI({
            find: sinon.stub().returns(Promise.resolve([{
                name: 'Fridhemsplan (Stockholm)',
                siteId: 9115,
                type: 'station',
                coords: null
            }])),
            realtime: sinon.stub().withArgs(9115).returns(Promise.resolve({
                departures: {
                    metro: [],
                    train: [
                        train(35, 'Spånga', '7 min'),
                        train(38, 'Uppsala C', '22:09',
                                       'Resa förbi Arlanda C kräver både UL- och SL- biljett.'),
                        train(36, 'Märsta', '25 min'),
                    ],
                    bus: [],
                    ship: []
                }
            })),
        });
        return cli.realtime('fridhemsplan')
        .then(data => assert.equal(data, [
            'Linje 35 mot Spånga - 7 min',
            'Linje 38 mot Uppsala C - 22:09' +
                ' (Resa förbi Arlanda C kräver både UL- och SL- biljett.)',
            'Linje 36 mot Märsta - 25 min'
        ].join('\n')));
    });
});
