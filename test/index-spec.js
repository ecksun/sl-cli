'use strict';

const assert = require('assert');

const nock = require('nock');

const realtimeFixture = require('./fixtures/realtime-centralen.json');
const findFixture = require('./fixtures/find-fridhem.json');

const run = require('../lib/sl-cli');

describe('sl-cli', function() {
    beforeEach(function() {
        nock('http://sl.se')
            .get(/Find/)
            .reply(200, findFixture);
        nock('http://sl.se')
            .get(/RealTime/)
            .reply(200, realtimeFixture);
    });
    it('should give only metro results given --metro all', function() {
        return run('http://sl.se', ['realtime', '--metro', 'all', 'fridhemsplan'])
        .then(o => assert.deepEqual(o, [
			'Tunnelbanans gröna linje 19 mot Hagsätra - Kort tåg - Nu',
			'Tunnelbanans gröna linje 18 mot Hässelby strand - 1 min',
			'Tunnelbanans röda linje 14 mot Mörby centrum - 2 min',
			'Tunnelbanans blå linje 10 mot Hjulsta - 2 min',
			'Tunnelbanans blå linje 11 mot Kungsträdgården - 3 min',
			'Tunnelbanans röda linje 13 mot Norsborg - 4 min',
			'Tunnelbanans röda linje 13 mot Ropsten - 4 min',
			'Tunnelbanans röda linje 14 mot Fruängen - 7 min',
			'Tunnelbanans blå linje 10 mot Kungsträdgården - 9 min',
			'Tunnelbanans röda linje 14 mot Mörby centrum - 11 min',
			'Tunnelbanans blå linje 11 mot Kungsträdgården - 13 min',
			'Tunnelbanans röda linje 13 mot Norsborg - 15 min'
        ].join('\n')));
    });
    it('should give blue metros results given --metro blue', function() {
        return run('http://sl.se', ['realtime', '--metro', 'blue', 'fridhemsplan'])
        .then(o => assert.deepEqual(o, [
			'Tunnelbanans blå linje 10 mot Hjulsta - 2 min',
			'Tunnelbanans blå linje 11 mot Kungsträdgården - 3 min',
			'Tunnelbanans blå linje 10 mot Kungsträdgården - 9 min',
			'Tunnelbanans blå linje 11 mot Kungsträdgården - 13 min',
        ].join('\n')));
    });
    it('should give busses only given --busses', function() {
        return run('http://sl.se', ['realtime', '--busses', 'fridhemsplan'])
        .then(o => {
            const nonBussLines = o.split('\n').filter(line => !line.startsWith('Buss '));
            assert(nonBussLines.length === 0, 'Got non-buss lines:\n' + nonBussLines.join('\n'));
        });
    });
});

