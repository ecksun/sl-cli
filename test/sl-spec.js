'use strict';

const assert = require('assert');
const sinon = require('sinon');

const realtimeFixture = require('./fixtures/realtime-centralen.json').data;
const findFixture = require('./fixtures/find-fridhem.json').data;
const SL = require('../lib/sl');
const MetroColor = SL.MetroColor;

const trainDeparture = require('./util').trainDeparture;
const metroDeparture = require('./util').metroDeparture;

const sl = new SL({
    find: sinon.stub().returns(Promise.resolve(findFixture)),
    realtime: sinon.stub().returns(Promise.resolve(realtimeFixture)),
});

describe('SL', function() {
    describe('find', function() {
        it('Parse search data', function() {
            return sl.find('fridhem')
            .then(data => assert.deepEqual(data, [{
                name: 'Fridhem (Ekerö)',
                siteId: 3157,
                type: 'station',
                coords: null
            }, {
                name: 'Fridhemsplan (Stockholm)',
                siteId: 9115,
                type: 'station',
                coords: null
            }, {
                name: 'Fridhem, Ösmo',
                siteId: 0,
                type: 'address',
                coords: '58.979125,17.966596'
            }]));
        });
    });
    describe('realtime', function() {
        it('should have the correct lastUpdate time', function() {
            return sl.realtime(1234)
            .then(data => assert.equal(data.lastUpdate, '17:22'));
        });
        it('should have the correct format of departures.metro', function() {
            return sl.realtime(1234)
            .then(data => assert.deepEqual(data.departures.metro, [
                metroDeparture(19, MetroColor.GREEN, 'Hagsätra - Kort tåg', 'Nu', ''),
                metroDeparture(18, MetroColor.GREEN, 'Hässelby strand', '1 min', ''),
                metroDeparture(14, MetroColor.RED, 'Mörby centrum', '2 min', ''),
                metroDeparture(10, MetroColor.BLUE, 'Hjulsta', '2 min', ''),
                metroDeparture(11, MetroColor.BLUE, 'Kungsträdgården', '3 min', ''),
                metroDeparture(13, MetroColor.RED, 'Norsborg', '4 min', ''),
                metroDeparture(13, MetroColor.RED, 'Ropsten', '4 min', ''),
                metroDeparture(14, MetroColor.RED, 'Fruängen', '7 min', ''),
                metroDeparture(10, MetroColor.BLUE, 'Kungsträdgården', '9 min', ''),
                metroDeparture(14, MetroColor.RED, 'Mörby centrum', '11 min', ''),
                metroDeparture(11, MetroColor.BLUE, 'Kungsträdgården', '13 min', ''),
                metroDeparture(13, MetroColor.RED, 'Norsborg', '15 min', ''),
            ]));
        });
        it('should include trains', function() {
            return sl.realtime(1234)
            .then(data => assert.deepEqual(data.departures.train, [
                trainDeparture(38, 'Älvsjö', 'Nu'),
                trainDeparture(36, 'Södertälje C', '8 min'),
                trainDeparture(35, 'Västerhaninge', '11 min'),
                trainDeparture(36, 'Södertälje C', '23 min'),
                trainDeparture(35, 'Västerhaninge', '26 min'),
                trainDeparture(38, 'Älvsjö', '17:53'),
                trainDeparture(36, 'Södertälje C', '18:01'),
                trainDeparture(35, 'Nynäshamn', '18:04'),
                trainDeparture(36, 'Södertälje C', '18:16'),
                trainDeparture(35, 'Västerhaninge', '18:19'),

                trainDeparture(35, 'Bålsta', '5 min'),
                trainDeparture(36, 'Märsta', '8 min'),
                trainDeparture(38, 'Uppsala C', '17:39',
                               'Resa förbi Arlanda C kräver både UL- och SL- biljett.'),
                trainDeparture(35, 'Kungsängen', '20 min'),
                trainDeparture(36, 'Märsta', '23 min'),
                trainDeparture(35, 'Bålsta', '17:58'),
                trainDeparture(36, 'Märsta', '18:01'),
                trainDeparture(38, 'Uppsala C', '18:09',
                               'Resa förbi Arlanda C kräver både UL- och SL- biljett.'),
                trainDeparture(35, 'Kungsängen', '18:13'),
                trainDeparture(36, 'Märsta', '18:16')
            ]));
        });
        it('should include train deviations', function() {
            return sl.realtime(1234)
            .then(data => {
                const uppsala = data.departures.train.filter(dep => dep.deviations.length > 0)[0];
                assert.deepEqual(uppsala.deviations, [{
                    text: 'Resa förbi Arlanda C kräver både UL- och SL- biljett.'
                }]);
            });
        });
    });
});
