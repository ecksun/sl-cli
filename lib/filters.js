'use strict';

module.exports.TypeFilter = (type) => dep => dep.type === type;
module.exports.MetroColorFilter = (color) => dep => dep.color === color;
