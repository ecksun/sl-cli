'use strict';

const MetroColor = {
    RED: Symbol('red'),
    GREEN: Symbol('green'),
    BLUE: Symbol('blue'),
    fromTransportSymbol(symbol) {
        switch (symbol) {
            case 'MET_red': return MetroColor.RED;
            case 'MET_green': return MetroColor.GREEN;
            case 'MET_blue': return MetroColor.BLUE;
        }
    }
};

const TravelType = {
    METRO: Symbol('metro'),
    BUS: Symbol('bus'),
    TRAIN: Symbol('train'),
    SHIP: Symbol('ship'),
};

module.exports = class SL {
    constructor(sl) {
        this.sl = sl;
    }
    find(q) {
        return this.sl.find(q)
        .then(results => results.map(result => ({
            name: result.Name,
            siteId: result.SiteId,
            type: result.Type,
            coords: result.Sid
        })));
    }
    realtime(siteId) {
        return this.sl.realtime(siteId)
        .then(res => {
            if (!process.env.DEBUG) {
                return res;
            }
            if (res.Error) {
                console.log('Response contained error', res.Error);
                console.log(res);
            }
            if (!res.HasResultData) {
                console.log('res.HasResultData === false');
                console.log(res);
            }
            if (res.HasStopPointDeviations) {
                console.log('HasStopPointDeviations');
                console.log(res);
            }
            return res;
        })
        // The Metro{Red,Green,Blue}Groups are probably not used anymore
        .then(res => ({
            departures: {
                metro: parseDepartures([].concat(
                    res.MetroRedGroups,
                    res.MetroGreenGroups,
                    res.MetroBlueGroups,
                    res.MetroGroups)),
                bus: parseBusGroups(res.BusGroups),
                train: parseTrainGroups(res.TrainGroups),
                ship: parseShipGroups(res.ShipGroups)
            },
            lastUpdate: res.LastUpdate
        }));
    }
};

module.exports.MetroColor = MetroColor;
module.exports.TravelType = TravelType;

function parseDepartures(departures) {
    const groups = departures
        .filter(e => !!e)
        .map(departure => departure.Departures);
    return [].concat.apply([], groups).map(parseDepature);
}

function parseDepature(data) {
    return {
        type: TravelType.METRO,
        line: data.GroupOfLine,
        destination: data.Destination,
        time: data.DisplayTime,
        lineNumber: data.LineNumber,
        message: data.PlatformMessage,
        color: MetroColor.fromTransportSymbol(data.TransportSymbol)
    };
}

function parseTrainGroups(groups) {
    const departures = groups.map(group => group.Departures);
    return [].concat.apply([], departures).map(parseTrainDeparture);
}

function parseTrainDeparture(group) {
    return {
        type: TravelType.TRAIN,
        lineNumber: group.LineNumber,
        time: group.DisplayTime,
        destination: group.Destination,
        deviations: group.Deviations.map(dev => ({ text: dev.Text }))
    };
}

function parseBusGroups(groups) {
    const departures = groups.map(group => group.Departures);
    return [].concat.apply([], departures).map(parseBusDeparture);
}

function parseBusDeparture(departure) {
    return {
        type: TravelType.BUS,
        lineNumber: departure.LineNumber,
        time: departure.DisplayTime,
        expected: departure.ExpectedDateTime,
        destination: departure.Destination
    };
}

function parseShipGroups(groups) {
    const departures = groups.map(group => group.Departures);
    return [].concat.apply([], departures).map(parseShipGroup);
}

// Entierly untested
function parseShipGroup(departure) {
    return {
        type: TravelType.SHIP,
        lineNumber: parseInt(departure.LineNumber),
        time: departure.DisplayTime,
        expected: departure.ExpectedDateTime,
        destination: departure.Destination
    };
}
