'use strict';

const SL = require('./sl');
const SLAPI = require('./sl-api');
const CLI = require('./cli');
const filters = require('./filters');

const packageJson = require('../package.json');

var ArgumentParser = require('argparse').ArgumentParser;

module.exports = function run(host, rawArgs) {
    var parser = new ArgumentParser({
        version: packageJson.version,
        addHelp: true,
        description: 'SL CLI',
    });

    var subparsers = parser.addSubparsers({
        title: 'subcommands',
        dest: 'subcommand'
    });

    var realtime = subparsers.addParser('realtime', { addHelp: true });
    realtime.addArgument('query', {
        action: 'store',
        help: 'Give relatime information for first hit for the given query'
    });

    const filterGroup = realtime.addMutuallyExclusiveGroup();
	filterGroup.addArgument(['-m', '--metros' ], {
		action: 'store',
        defaultValue: false,
        choices: ['all', 'blue', 'green', 'red'],
		help: 'Show only metros for the given color'
	});
	filterGroup.addArgument(['-b', '--busses' ], {
		action: 'storeTrue',
		help: 'Show only busses'
	});
	filterGroup.addArgument(['-t', '--trains' ], {
		action: 'storeTrue',
		help: 'Show only trains'
	});

    var find = subparsers.addParser('search', { addHelp: true });
    find.addArgument('query', {
        action: 'store',
        help: 'Find things matching the query'
    });

    var args = parser.parseArgs(rawArgs);

    const cli = new CLI(new SL(new SLAPI(host)));

    if (args.subcommand === 'realtime') {
        return cli.realtime(args.query, getRealtimeFilter(args));
    }
    else if (args.subcommand === 'search') {
        return cli.find(args.query);
    }
};

function getRealtimeFilter(args) {
    const filterUnion = [];
    if (args.metros) {
        filterUnion.push(getMetrofilter(args.metros));
    }
    if (args.busses) {
        filterUnion.push(filters.TypeFilter(SL.TravelType.BUS));
    }
    if (args.trains) {
        filterUnion.push(filters.TypeFilter(SL.TravelType.TRAIN));
    }

    if (filterUnion.length) {
        return (dep) => filterUnion.find(filter => filter(dep));
    }
    return () => true;
}

function getMetrofilter(color) {
    switch (color) {
        case 'blue': return filters.MetroColorFilter(SL.MetroColor.BLUE);
        case 'green': return filters.MetroColorFilter(SL.MetroColor.GREEN);
        case 'red': return filters.MetroColorFilter(SL.MetroColor.RED);
        default: return filters.TypeFilter(SL.TravelType.METRO);
    }
}
