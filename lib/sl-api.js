'use strict';

const request = require('request');

module.exports = class SLAPI {
    constructor(host) {
        this.host = host;
    }
    query(url) {
        return new Promise(function(resolve, reject) {
            request.get({
                url: url,
                json: true
            }, function(err, res, body) {
                if (err) {
                    return reject(err);
                }
                resolve(body);
            });
        });
    }
    find(q) {
        return this.query(this.host + '/api/TypeAhead/Find/' + q)
        .then(data => data.data);
    }
    realtime(siteId) {
        return this.query(this.host + '/api/sv/RealTime/GetDepartures/' + siteId)
        .then(data => data.data);
    }
};
