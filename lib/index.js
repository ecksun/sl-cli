#!/usr/bin/env node
'use strict';

const run = require('./sl-cli');

run('http://sl.se', process.argv.slice(2))
    .then(o => console.log(o))
    .catch(e => console.error(e));
