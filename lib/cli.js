'use strict';

const TravelType = require('./sl').TravelType;

const capitalize = s => `${s[0].toUpperCase()}${s.slice(1)}`;

module.exports = class CLI {
    constructor(sl) {
        this.sl = sl;
    }
    realtime(q, givenFilter) {
        const filter = givenFilter || (() => true);
        return this.sl.find(q)
        .then(results => results[0].siteId)
        .then(siteId => this.sl.realtime(siteId))
        .then(res => [].concat(
            res.departures.metro,
            res.departures.bus,
            res.departures.train,
            res.departures.ship
        ))
        .then(departures => departures.filter(filter))
        .then(departures => [].concat(
            departures
                .filter(dep => dep.type === TravelType.METRO)
                .map(dep => {
                    const message = dep.message ? ` (${dep.message})` : '';
                    const line = capitalize(dep.line);
                    return `${line} ${dep.lineNumber} mot ` +
                        `${dep.destination} - ${dep.time}${message}`;
                }),
            departures
                .filter(dep => dep.type === TravelType.TRAIN)
                .map(dep => {
                    const deviations = dep.deviations.map(dev => dev.text);
                    const message = deviations.length  ? ` (${deviations.join(' ')})` : '';
                    return `Linje ${dep.lineNumber} mot ${dep.destination} - ${dep.time}${message}`;
                }),
            departures
                .filter(dep => dep.type === TravelType.BUS)
                .map(dep => {
                    return `Buss ${dep.lineNumber} mot ${dep.destination} - ${dep.time}`;
                })
        ).join('\n'));
    }
    find(q) {
        return this.sl.find(q)
        .then(res => {
            return JSON.stringify(res);
        });
    }
};
